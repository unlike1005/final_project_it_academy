import random
import json


TOKEN = "0f34515768b5eae0a64510c20b80e8ca3971295f16b49bae5693fed8206af2b6"
number = random.randrange(1, 10000)


def main():
    id_user = str(random.randrange(1, 1400))
    data = {
        "token": TOKEN,
        "url_base": "https://gorest.co.in/public-api/",
        "url_users": "users/",
        "url_one_user": f'{id_user}/',
        "url_posts": "posts/",
        "url_comments": "comments/",
        "url_page": "?page=",
        "url_todos": "todos/",
        "headers": {
            "Accept": "application/json",
            "Authorization": f"Bearer {TOKEN}"
        },
        "headers_incorrect_token": {
            "Accept": "application/json",
            "Authorization": f"Bearer {TOKEN}q"
        },
        "data": [
            {"name": "Alla P",
             "gender": "Female",
             "email": f"unlike{number}@oil.com",
             "status": "Active"
             },
            {"name": "Petrov Petr",
             "gender": "Male",
             "email": f"unlike_petrov{number}@oil.com",
             "status": "Inactive"
             }
        ],
        "data_incorrect": [
            {"name": "",
             "gender": "Female",
             "email": f"unlike{number}@oil.com",
             "status": "Active"
             },
            {"name": "Alla P",
             "gender": "",
             "email": f"unlike{number}@oil.com",
             "status": "Active"
             },
            {"name": "Alla P",
             "gender": "Female",
             "email": "",
             "status": "Active"
             },
            {"name": "Alla P",
             "gender": "Female",
             "email": f"unlike{number}@oil.com",
             "status": ""
             },
            {"name": "Alla P",
             "gender": "Female",
             "email": f"unlike{number}@oil.com",
             "status": "some"
             },
            {"name": "Alla P",
             "gender": "FemaleMale",
             "email": f"unlike{number}@oil.com",
             "status": "Active"
             },
            {"name": "Alla P",
             "gender": "Female",
             "email": "unlike)@oil.com",
             "status": "Active"
             },
            {"name": "Alla P",
             "gender": "Female",
             "email": "unlike(@oil.com",
             "status": "Active"
             },
            {"name": "Alla P",
             "gender": "Female",
             "email": "unlike@uli@oil.com",
             "status": "Active"
             },
            {"name": "Alla P",
             "gender": "Female",
             "email": "unlike<uli@oil.com",
             "status": "Active"
             },
            {"name": "Alla P",
             "gender": "Female",
             "email": "unlike>uli@oil.com",
             "status": "Active"
             },
            {"name": "Alla P",
             "gender": "Female",
             "email": "unlike,uli@oil.com",
             "status": "Active"
             },
            {"name": "Alla P",
             "gender": "Female",
             "email": "unlike;uli@oil.com",
             "status": "Active"
             }
        ],
        "data_update_patch": {
            "name": f"Ivan Ivanov{id_user}patch",
            "gender": "Male",
            "email": f"unlike{number}{id_user}patch@oil.com",
            "status": "Active"
        },
        "data_update_put": {
            "name": f"Ivan Ivanov{id_user}put",
            "gender": "Male",
            "email": f"unlike{number}{id_user}put@oil.com",
            "status": "Active"
        },
        "data_one_user_post": {
            "title": f"some title user id {id_user}",
            "body": "It is body of some title."
        },
        "data_one_user_comment": {
            "name": "Alla P",
            "email": f"unlike{number}@oil.com",
            "body": "It is body of some message."
        },
        "data_todo": {
            "title": "one title",
            "completed": "True"
        },
        "data_todo_completed_false":
            {"title": "two title",
             "completed": "False"
             },
        "data_incorrect_todo1": {
            "title": "three title",
            "completed": ""
        },
        "data_incorrect_todo2": [
            {"title": "four title",
             "completed": " "
             },
            {"title": "four title",
             "completed": "it is incorrect completed"
             }
        ]
    }
    with open('data.json', 'w') as f:
        json.dump(data, f, indent=4)
    return id_user
