import pytest
import requests
import json
import datetime
import re
from data_for_json import main
from tzlocal import get_localzone
from urllib.parse import urljoin


def get_datas():
    get_random_user = False
    count = 0
    while get_random_user is False:
        count += 1
        print(count)
        id_user = main()
        with open("data.json", "r", encoding='utf-8') as f:
            datas = json.load(f)
        url_base = datas.get("url_base")
        url_users = urljoin(url_base, datas.get("url_users"))
        url_one_user = datas.get("url_one_user")
        headers = datas.get("headers")
        response = requests.get(urljoin(url_users, url_one_user),
                                headers=headers).json()
        if response.get("code") != 404:
            get_random_user = True
    return datas, url_base, url_users, url_one_user, headers, id_user


datas, url_base, url_users, url_one_user, headers, id_user = get_datas()
url_posts = datas.get("url_posts")
url_comments = datas.get("url_comments")
url_page = datas.get("url_page")
url_todos = datas.get("url_todos")
url_users_one_user = urljoin(url_users, url_one_user)


def inspect_date(response_date):
    date = datetime.datetime.strptime(response_date, '%Y-%m-%dT%H:%M:%S.%f%z')
    time_now = datetime.datetime.now(tz=get_localzone())
    delta = date - time_now
    print(date)
    print(time_now)
    print(delta)
    assert delta < datetime.timedelta(days=0, seconds=10, microseconds=0,
                                      milliseconds=0, minutes=0, hours=0)


# INCORRECT TOKEN ---------------------------------------------------------
def test_incorrect_token():
    response = requests.post(url_users,
                             headers=datas.get("headers_incorrect_token"),
                             data=datas.get("data")[0]).json()
    assert response.get("code") == 401


# CREATE USER NEGATIVE ----------------------------------------------------
@pytest.mark.parametrize("data", datas.get("data_incorrect"))
def test_create_user_negative(data):
    response = requests.post(url_users, headers=headers, data=data).json()
    assert response.get("code") == 422


# CREATE USER POSITIVE AND GET USER ---------------------------------------
def asserts_for_test_create_or_get_user(datas_data, response):
    assert datas_data.get("name") == response.get("name")
    assert datas_data.get("gender") == response.get("gender")
    assert datas_data.get("email") == response.get("email")
    assert datas_data.get("status") == response.get("status")
    inspect_date(response.get("created_at"))
    assert response.get("created_at") == response.get("updated_at")


@pytest.mark.parametrize("datas_data", datas.get("data"))
def test_create_user_positive(datas_data):
    response = requests.post(
        url_users, headers=headers, data=datas_data).json()
    print(url_users, response)
    assert response.get("code") == 201
    asserts_for_test_create_or_get_user(datas_data, response.get("data"))


def test_create_user_second_time():
    response1 = requests.post(
        url_users, headers=headers, data=datas.get("data")[0]).json()
    assert response1.get("code") == 422


def test_get_one_user():
    response = requests.get(url_users, headers=headers,
                            data=datas.get("data")[0]).json()
    assert response.get("code") == 200
    assert ((response.get("meta")).get("pagination")).get("total") == 1
    asserts_for_test_create_or_get_user(datas.get("data")[0],
                                        response.get("data")[0])


# UPDATE USER -------------------------------------------------------------
def for_update_user(query):
    data_update = datas.get(query)
    if "put" in query:
        response = requests.put(url_users_one_user,
                                headers=headers,
                                data=data_update).json()
    elif "patch" in query:
        response = requests.patch(url_users_one_user,
                                  headers=headers,
                                  data=data_update).json()
    assert response.get("code") == 200
    assert data_update.get("name") == response.get("data").get("name")
    assert data_update.get("email") == response.get("data").get("email")
    assert data_update.get("gender") == response.get("data").get("gender")
    assert data_update.get("status") == response.get("data").get("status")
    inspect_date(response.get("data").get("updated_at"))
    assert datetime.datetime.strptime(
        response.get("data").get("created_at"),
        '%Y-%m-%dT%H:%M:%S.%f%z').date() <= datetime.datetime.strptime(
        response.get("data").get("updated_at"),
        '%Y-%m-%dT%H:%M:%S.%f%z').date()
    return response.get("code")


def test_update_user_patch():
    for_update_user(query="data_update_patch")


def test_update_user_put():
    for_update_user(query="data_update_put")


# POSTS-------------------------------------------------------------------
def get_post(return_code=True):
    response = requests.get(urljoin(url_users_one_user, url_posts),
                            headers=headers).json()
    last_post = (response.get("data"))[len(response.get("data")) - 1]
    if return_code:
        return response.get("code"), last_post
    else:
        return last_post


def get_id_post():
    last_post = get_post(return_code=False)
    id_post = str(last_post.get("id")) + "/"
    return id_post


def asserts_for_post(some_post):
    assert some_post.get("user_id") == int(id_user)
    assert datas.get("data_one_user_post").get("title") == \
           some_post.get("title")
    assert datas.get("data_one_user_post").get("body") == some_post.get("body")


def test_create_post():
    response = requests.post(urljoin(url_users_one_user, url_posts),
                             headers=headers,
                             data=datas.get("data_one_user_post")).json()
    assert response.get("code") == 201
    asserts_for_post(response.get("data"))
    inspect_date(response.get("data").get("created_at"))
    assert response.get("created_at") == response.get("updated_at")


def test_get_post():
    code, last_post = get_post()
    assert code == 200
    asserts_for_post(last_post)


# COMMENTS----------------------------------------------------------------
def asserts_for_comments(some_comment, id_post):
    assert some_comment.get("post_id") == int(id_post[:-1])
    assert some_comment.get("name") == datas.get(
        "data_one_user_comment").get("name")
    assert some_comment.get("email") == datas.get(
        "data_one_user_comment").get("email")
    assert some_comment.get("body") == datas.get(
        "data_one_user_comment").get("body")
    inspect_date(some_comment.get("created_at"))
    assert some_comment.get("created_at") == some_comment.get("updated_at")


def actions_for_test_comment(code):
    id_post = get_id_post()
    url = urljoin(urljoin(urljoin(url_base, url_posts), id_post), url_comments)
    if code == 200:
        response = requests.get(url, headers=headers).json()
        asserts_for_comments(response.get("data")[-1], id_post)
    elif code == 201:
        response = requests.post(
            url, headers=headers,
            data=datas.get("data_one_user_comment")).json()
        asserts_for_comments(response.get("data"), id_post)
    assert response.get("code") == code


def test_create_comment():
    actions_for_test_comment(201)


def test_get_comments():
    actions_for_test_comment(200)


# _TODO--------------------------------------------------------------------
def asserts_for_create_and_get_todo(response, data):
    assert response.get("user_id") == int(url_one_user[:-1])
    assert response.get("title") == data.get("title")
    assert str(response.get("completed")) == data.get("completed")
    inspect_date(response.get("created_at"))
    assert response.get("created_at") == response.get("updated_at")


def actions_for_create_todo(data):
    response = requests.post(urljoin(url_users_one_user, url_todos),
                             headers=headers, data=data).json()
    assert response.get("code") == 201
    asserts_for_create_and_get_todo(response.get("data"), data)


def test_create_todo_with_completed_is_true():
    actions_for_create_todo(datas.get("data_todo"))


def test_get_todo():
    response = requests.get(urljoin(url_users_one_user, url_todos),
                            headers=headers).json()
    assert response.get("code") == 200
    asserts_for_create_and_get_todo(response.get("data")[-1],
                                    datas.get("data_todo"))


@pytest.mark.xfail(reason="created with completed is true. bug.")
def test_create_todo_created_with_completed_is_false():
    actions_for_create_todo(datas.get("data_todo_completed_false"))


def test_create_todo_with_empty_completed():
    response = requests.post(urljoin(url_users_one_user, url_todos),
                             headers=headers,
                             data=datas.get("data_incorrect_todo1")).json()
    assert response.get("code") == 422


@pytest.mark.xfail(reason="return code 201. bug.")
@pytest.mark.parametrize("data", datas.get("data_incorrect_todo2"))
def test_create_todo_with_incorrect_completed(data):
    response = requests.post(urljoin(url_users_one_user, url_todos),
                             headers=headers, data=data).json()
    assert response.get("code") == 422


# GET ALL USERS, POSTS, COMMENTS, TODOS -----------------------------------
def get_all_somethings(url, url_last_pages=True):
    response = requests.get(url, headers=headers).json()
    pages = response.get("meta").get("pagination").get("pages")
    assert response.get("code") == 200
    assert response.get("meta").get("pagination").get("total") >= 1
    assert pages >= 1
    assert datetime.datetime.strptime(
        response.get("data")[0].get("created_at"),
        '%Y-%m-%dT%H:%M:%S.%f%z').date() <= datetime.datetime.strptime(
        response.get("data")[0].get("updated_at"),
        '%Y-%m-%dT%H:%M:%S.%f%z').date()
    if url_last_pages is True:
        if pages > 1:
            assert len(response.get("data")) == \
                   response.get("meta").get("pagination").get("limit")
        url_last_page = urljoin(url, url_page + str(pages))
        return response, url_last_page
    return response


def for_all_users(data):
    assert data.get("name") != ""
    assert data.get("gender") == "Male" or data.get("gender") == "Female"
    assert re.search(
        r"^[a-zA-Z][a-zA-Z0-9_+%#=|^!?.`\\&*-]*[@][0-9]*[a-z-]*[0-9]*\."
        r"[a-z]+$",
        data.get("email"))
    assert data.get("status") == "Active" or data.get("status") == "Inactive"


def test_get_all_users():
    response, url_last_page = get_all_somethings(url_users)
    for_all_users(response.get("data")[0])
    response_last_page = get_all_somethings(url_last_page, False)
    for_all_users(response_last_page.get("data")[-1])


def for_get_all_posts(data):
    assert data.get("user_id") >= 1
    assert data.get("title") != ""
    assert data.get("body") != ""


def test_get_all_posts():
    url_first_page = urljoin(url_base, url_posts)
    response, url_last_page = get_all_somethings(url_first_page)
    for_get_all_posts(response.get("data")[0])
    response_last_page = get_all_somethings(url_last_page, False)
    for_get_all_posts(response_last_page.get("data")[-1])


def for_get_all_comments(data):
    assert data.get("id") != ""
    assert data.get("post_id") != ""
    assert data.get("name") != ""
    assert data.get("email") != ""
    assert data.get("body") != ""


def test_get_all_comments():
    url_first_page = urljoin(url_base, url_comments)
    response, url_last_page = get_all_somethings(url_first_page)
    for_get_all_comments(response.get("data")[0])
    response_last_page = get_all_somethings(url_last_page, False)
    for_get_all_comments(response_last_page.get("data")[-1])


def for_get_all_todos(data):
    assert data.get("id") != ""
    assert data.get("user_id") != ""
    assert data.get("title") != ""
    assert data.get("completed") != ""


def test_get_all_todos():
    url_first_page = urljoin(url_base, url_todos)
    response, url_last_page = get_all_somethings(url_first_page)
    for_get_all_todos(response.get("data")[0])
    response_last_page = get_all_somethings(url_last_page, False)
    for_get_all_todos(response_last_page.get("data")[-1])


# DELETE------------------------------------------------------------------
def test_delete_user():
    response = requests.delete(url_users_one_user,
                               headers=headers).json()
    assert response.get("code") == 204
    response1 = requests.get(url_users_one_user,
                             headers=headers).json()
    assert response1.get("code") == 404
