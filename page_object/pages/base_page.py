from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from page_object.locators.base_page_locators import BasePageLocators


class BasePage:
    def __init__(self, driver, url):
        self.driver = driver
        self.driver.implicitly_wait(10)
        self.url = url

    def open(self, url):
        self.driver.get(url)

    def find_clickable_element(self, locate_by, selector):
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((locate_by, selector)))
            return element
        except TimeoutException:
            return False

    def find_text_element(self, locate_by, selector):
        try:
            element = self.driver.find_element(locate_by, selector)
            return element.text
        except NoSuchElementException:
            return False

    def find_some_elements(self, locate_by, selector):
        try:
            elements = self.driver.find_elements(locate_by, selector)
            print(elements)
            return elements
        except NoSuchElementException:
            return False

    def find_text_elements(self, locate_by, selector):
        try:
            elements = self.driver.find_elements(locate_by, selector)
            texts_in_elements = []
            for i in elements:
                texts_in_elements.append(i.text)
            return texts_in_elements
        except NoSuchElementException:
            return False

    def should_be_login_link(self):
        return self.find_clickable_element(*BasePageLocators.LOGIN_LINK)

    def should_be_basket_button(self):
        return self.find_clickable_element(*BasePageLocators.BASKET_BUTTON)

    def should_be_contact_link(self):
        return self.find_clickable_element(*BasePageLocators.CONTACT_US_LINK)

    def should_be_search(self):
        search_form = \
            self.find_clickable_element(*BasePageLocators.SEARCH_FORM)
        search_button = \
            self.find_clickable_element(*BasePageLocators.SEARCH_BUTTON)
        return search_form, search_button

    def find_displayed_elements(self, locate_by, selector):
        elements = self.driver.find_elements(locate_by, selector)
        print(f"find_displayed_elements ВСЕГО НАЙДЕНО ЭЛЕМЕНТОВ"
              f" {len(elements)}")
        displayed_elements = []
        for i in range(len(elements)):
            if elements[i].is_displayed():
                displayed_elements.append(elements[i])
            i += 1
        print(f"find_displayed_elements ИЗ НИХ ВИДИМЫХ ЭЛЕМЕНТОВ"
              f" {len(displayed_elements)}")
        return displayed_elements

    @staticmethod
    def str_to_float(*sums):
        list_args = []
        for i in sums:
            try:
                list_args.append(float(i))
            except ValueError or TypeError:
                list_args.append(False)
        sums_float = tuple(list_args)
        return sums_float
