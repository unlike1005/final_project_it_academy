from page_object.locators.basket_page_locators import BasketPageLocators
from page_object.pages.base_page import BasePage
from selenium.common.exceptions import ElementClickInterceptedException
# import time


class BasketPage(BasePage):

    def should_be_elements_on_busket_page(self):
        home_button = self.should_be_home_button()
        shopping_cart = self.should_be_shopping_cart()
        cart_summary = self.should_be_shopping_cart_summary()
        steps = self.should_be_steps()
        empty_warning = self.should_be_empty_warning()
        return home_button, shopping_cart, cart_summary, steps, empty_warning

    def should_be_home_button(self):
        return self.find_clickable_element(*BasketPageLocators.HOME_BUTTON)

    def should_be_shopping_cart(self):
        return self.find_text_element(*BasketPageLocators.SHOPPING_CART)

    def should_be_shopping_cart_summary(self):
        return self.find_text_element(
            *BasketPageLocators.SHOPPING_CART_SUMMARY)

    def should_be_steps(self):
        return self.find_clickable_element(*BasketPageLocators.STEPS)

    def should_be_empty_warning(self):
        return self.find_text_element(*BasketPageLocators.EMPTY_WARNING)

    def should_be_product_name(self):
        return self.find_text_element(*BasketPageLocators.PRODUCT_NAME)

    def should_by_prices_and_quantity_on_basket_page(self, need_tax=True):
        unit_price = self.should_be_unit_price()
        qty = self.should_be_qty()
        total_one_name = self.should_be_total_one_name()
        total_products = self.should_be_total_products()
        total_shipping = self.should_be_total_shipping()
        total_without_tax = self.should_be_total_without_tax()
        if need_tax is True:
            tax = self.should_be_tax()
            total_with_tax = self.should_be_total_with_tax()
            return (unit_price, qty, total_one_name, total_products,
                    total_shipping, total_without_tax, tax, total_with_tax)
        return (unit_price, qty, total_one_name, total_products,
                total_shipping, total_without_tax)

    def should_be_unit_price(self):
        return self.find_text_elements(*BasketPageLocators.UNIT_PRICE)

    def should_be_qty(self):
        qty = self.find_some_elements(*BasketPageLocators.QTY)
        if qty is not False:
            values_qwe = []
            for i in qty:
                values_qwe.append(i.get_attribute("value"))
            return values_qwe
        return False

    def should_be_total_one_name(self):
        return self.find_text_elements(*BasketPageLocators.TOTAL_ONE_NAME)

    def should_be_total_products(self):
        return self.find_text_element(*BasketPageLocators.TOTAL_PRODUCTS)

    def should_be_total_shipping(self):
        return self.find_text_element(*BasketPageLocators.TOTAL_SHIPPING)

    def should_be_total_without_tax(self):
        return self.find_text_element(*BasketPageLocators.TOTAL_WITHOUT_TAX)

    def should_be_tax(self):
        return self.find_text_element(*BasketPageLocators.TAX)

    def should_be_total_with_tax(self):
        return self.find_text_element(*BasketPageLocators.TOTAL_WITH_TAX)

    def should_be_icon_plus(self):
        return self.find_some_elements(*BasketPageLocators.ICON_PLUS)

    def should_be_icon_minus(self):
        return self.find_some_elements(*BasketPageLocators.ICON_MINUS)

    def should_be_delete_product_buttons(self):
        return self.find_some_elements(
            *BasketPageLocators.DELETE_PRODUCT_BUTTONS)

    @staticmethod
    def click_to_delete_button(delete_product_buttons):
        for i in delete_product_buttons:
            try:
                i.click()
            except ElementClickInterceptedException:
                return False
        return True

    @staticmethod
    def click_on_icon_plus(icon_plus):
        try:
            icon_plus.click()
            return True
        except ElementClickInterceptedException:
            return False

    @staticmethod
    def click_on_icon_minus(icon_minus):
        try:
            icon_minus.click()
            return True
        except ElementClickInterceptedException:
            return False

    def checking_math(self):
        result = self.should_by_prices_and_quantity_on_basket_page(
            need_tax=True)
        assert False not in result
        (unit_price, qty, total_one_name, total_products, total_shipping,
         total_without_tax, tax, total_with_tax) = result
        assert len(unit_price) == len(qty) == len(total_one_name)
        (total_products, total_shipping, total_without_tax, tax,
         total_with_tax) = self.str_to_float(total_products[1:],
                                             total_shipping[1:],
                                             total_without_tax[1:], tax[1:],
                                             total_with_tax[1:])
        print(f'total_one_name: {total_one_name}')
        i = 0
        total_one_name_list = []
        qty_list = []
        while i < len(unit_price):
            unit_price_float, qty_float, total_one_name_float = \
                self.str_to_float(unit_price[i][1:], qty[i],
                                  total_one_name[i][1:])
            assert unit_price_float * qty_float == total_one_name_float
            total_one_name_list.append(total_one_name_float)
            qty_list.append(qty_float)
            i += 1
        print(total_one_name_list, sum(total_one_name_list), total_products)
        assert round(sum(total_one_name_list), 2) == total_products
        assert total_products + total_shipping == total_without_tax
        assert total_without_tax + tax == total_with_tax
        return qty_list
