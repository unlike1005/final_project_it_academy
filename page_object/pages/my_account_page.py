from selenium.common.exceptions import ElementClickInterceptedException
from page_object.locators.my_accaunt_page_locators \
    import MyAccountLocators
from page_object.pages.base_page import BasePage
from page_object.pages.wishlists_page import WishListsPage


class MyAccountPage(BasePage):
    def should_be_text_my_account_up(self):
        return self.find_text_element(
            *MyAccountLocators.TEXT_MY_ACCOUNT_UP)

    def should_be_text_my_account_down(self):
        return self.find_text_element(
            *MyAccountLocators.TEXT_MY_ACCOUNT_DOWN)

    def should_be_text_info_account(self):
        return self.find_text_element(
            *MyAccountLocators.INFO_ACCOUNT)

    def should_be_order_button(self):
        return self.find_clickable_element(*MyAccountLocators.ORDER_BUTTON)

    def should_be_credit_button(self):
        return self.find_clickable_element(*MyAccountLocators.CREDIT_BUTTON)

    def should_be_address_button(self):
        return self.find_clickable_element(*MyAccountLocators.ADDRESS_BUTTON)

    def should_be_personal_info_button(self):
        return self.find_clickable_element(
            *MyAccountLocators.PERSONAL_INFO_BUTTON)

    def should_be_wishlists_button(self):
        return self.find_clickable_element(*MyAccountLocators.WISHLISTS_BUTTON)

    def should_be_home_button_down(self):
        return self.find_clickable_element(*MyAccountLocators.HOME_BUTTON_DOWN)

    def click_on_wishlists_button(self, wishlists_button):
        try:
            wishlists_button.click()
            self.driver.switch_to.parent_frame()
            return WishListsPage(self.driver, self.driver.current_url)
        except ElementClickInterceptedException:
            return False

    # def click_on_home_button(self, home_button):
    #     try:
    #         home_button.click()
    #         return MainPage(self.driver, self.driver.current_url)
    #     except ElementClickInterceptedException:
    #         return False
