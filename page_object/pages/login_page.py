from page_object.locators.login_page_locators import LoginPageLocators
from page_object.pages.base_page import BasePage
from page_object.pages.create_account_page import CreateAccountPage
from page_object.pages.my_account_page import MyAccountPage


class LoginPages(BasePage):

    def should_be_login_page(self):
        login_button, email_login_form, password_login_form = \
            self.login_elements()
        msg = self.find_text_element(*LoginPageLocators.MSG1)
        create_account_button, email_create_account_form = \
            self.register_elements()
        return login_button, email_login_form, password_login_form, \
            msg, create_account_button, email_create_account_form

    def get_url(self):
        return self.driver.current_url

    def find_login_button(self):
        login_button = \
            self.find_clickable_element(*LoginPageLocators.SIGN_IN_BUTTON)
        return login_button

    def login_elements(self):
        login_button = self.find_login_button()
        email_login_form = \
            self.find_clickable_element(*LoginPageLocators.EMAIL_LOGIN_FORM)
        password_login_form = \
            self.find_clickable_element(*LoginPageLocators.PASSWORD_LOGIN_FORM)
        return login_button, email_login_form, password_login_form

    def register_elements(self):
        create_account_button = self.find_clickable_element(
            *LoginPageLocators.CREATE_ACCOUNT_BUTTON)
        email_create_account_form = self.find_clickable_element(
            *LoginPageLocators.EMAIL_CREATE_ACCOUNT_FORM)
        return create_account_button, email_create_account_form

    def create_account(self, email):
        # self.driver.get(self.url)
        create_account_button, email_create_account_form = \
            self.register_elements()
        email_create_account_form.send_keys(email)
        create_account_button.click()

        error_msg1 = self.find_text_element(*LoginPageLocators.ERROR_MSG1)
        if error_msg1 is not False:
            return error_msg1, self.get_url()
        else:
            return CreateAccountPage(self.driver, self.get_url())

    def login(self, email, password):
        self.driver.get(self.url)
        login_button, email_login_form, password_login_form = \
            self.login_elements()
        email_login_form.send_keys(email)
        password_login_form.send_keys(password)
        login_button.click()

        error_msg2 = self.find_text_element(*LoginPageLocators.ERROR_MSG2)
        error_msg3 = self.find_text_element(*LoginPageLocators.ERROR_MSG3)
        if error_msg2 is not False and error_msg3 is not False:
            return error_msg2, error_msg3, self.get_url()
        elif error_msg2 is False and error_msg3 is False:
            return MyAccountPage(self.driver, self.get_url())
        else:
            return "Error: one of elements is False." \
                   " Function login_page.login maybe need to correct."

    # def should_be_logo_link(self):
    #     return self.find_clickable_element(*LoginPageLocators.LOGO_LINK)
    #
    # def click_logo_link(self, logo_link):
    #     logo_link.click()
    #     return MainPage(self.driver, self.driver.current_url)
