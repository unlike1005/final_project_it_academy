from page_object.locators.main_page_locators import MainPageLocators
from page_object.pages.base_page import BasePage
from page_object.pages.login_page import LoginPages
from page_object.pages.basket_page import BasketPage
from page_object.pages.contact_page import ContactPage
from page_object.pages.search_page import SearchPage
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.webdriver import ActionChains
import random
import time


class MainPage(BasePage):

    def open_login_page(self, login_link):
        try:
            login_link.click()
            return LoginPages(self.driver, self.driver.current_url)
        except ElementClickInterceptedException:
            return False

    def open_basket_page(self, basket_button):
        try:
            basket_button.click()
            self.driver.switch_to.parent_frame()
            return BasketPage(self.driver, self.driver.current_url)
        except ElementClickInterceptedException:
            return False

    def open_basket_page_on_alert(self, basket_button):
        try:
            basket_button.click()
            self.driver.switch_to.parent_frame()
            return BasketPage(self.driver, self.driver.current_url)
        except ElementClickInterceptedException:
            return False

    def open_contact_page(self, contact_link):
        try:
            contact_link.click()
            return ContactPage(self.driver, self.driver.current_url)
        except ElementClickInterceptedException:
            return False

    def open_search_page(self, search_form, search_button):
        try:
            search_form.send_keys("blouse")
            search_button.click()
            return SearchPage(self.driver, self.driver.current_url)
        except ElementClickInterceptedException:
            return False

    def set_size_window(self):
        self.driver.set_window_position(0, 0)
        self.driver.set_window_size(1420, 1070)

    @staticmethod
    def one_product_from_list(list_objects, text=True):
        index_one_product = random.randint(0, len(list_objects) - 1)
        one_element = list_objects[index_one_product]
        if text is True:
            return one_element, one_element.text
        else:
            return one_element

    def activate_view(self, some_object):
        self.driver.execute_script("arguments[0].scrollIntoView();",
                                   some_object)
        action = ActionChains(self.driver)
        action.move_to_element(some_object).perform()

    def go_to_alert_form(self, element_on_main_page):
        print(f'ЭЛЕМЕНТ С ГЛАВНОЙ СТРАНИЦЫ: {element_on_main_page}')
        self.activate_view(element_on_main_page)
        time.sleep(2)
        add_to_cart_button = self.should_be_add_to_cart_button()
        print(f"ДЛИНА СПИСКА НАЙДЕННЫХ АКТИВНЫХ КНОПОК:"
              f" {len(add_to_cart_button)}")
        assert len(add_to_cart_button) == 1
        add_to_cart_button[0].click()
        time.sleep(2)
        self.driver.switch_to_alert
        time.sleep(2)

    def go_to_continue_shopping(self):
        continue_button = self.should_be_continue_button_on_alert()
        print(f"НАЙДЕНО КНОПОК ДЛЯ ПРОДОЛЖЕНИЯ ПОКУПОК: {continue_button}")
        continue_button.click()
        time.sleep(2)
        self.driver.switch_to.parent_frame()

    def should_be_products(self):
        self.set_size_window()

        imgs = self.should_be_img_products()
        names = self.should_be_name_products()
        prices_under_img = self.should_be_price_products_under_img()

        price_one_product_under_img, price_under_img_text =\
            self.one_product_from_list(prices_under_img)
        self.activate_view(price_one_product_under_img)

        view = self.should_be_view()
        price_on_img = self.should_be_price_products_on_img()
        add_to_cart_button = self.should_be_add_to_cart_button()
        more_button = self.should_be_more_button()

        return (imgs, names, prices_under_img, view, price_on_img,
                add_to_cart_button, more_button, price_under_img_text)

    @staticmethod
    def delete_overflow_symbols(any_strings):
        possible_symbols = "0123456789."
        new_array_of_strings = []
        for some_string in any_strings:
            new_string = ""
            for i in some_string:
                if i in possible_symbols:
                    new_string += i
            new_array_of_strings.append(new_string)
        return tuple(new_array_of_strings)

    def one_element_on_main_page_and_alert(self, locator1, locator2, count,
                                           need_quantity=False,
                                           need_basket_quantity=False,
                                           only_quantity=False):
        self.set_size_window()
        if need_basket_quantity is True:
            empty_basket = self.should_be_empty_basket()
        elements_on_main_page = self.find_displayed_elements(*locator1)
        element_on_main_page, element_on_main_page_text =\
            self.one_product_from_list(elements_on_main_page)
        try:
            i = 0
            while i < count - 1:
                self.go_to_alert_form(element_on_main_page)
                self.go_to_continue_shopping()
                i += 1
            self.go_to_alert_form(element_on_main_page)
            element_on_alert_text = self.find_text_element(*locator2)
            print(f"ТОТАЛ ЛЕВЫЙ: {element_on_alert_text}")
            if need_quantity is True:
                quantity_str = self.should_be_quantity_on_alert()
                print(f"КОЛИЧЕСТВО: {quantity_str}")
                if only_quantity is True:
                    quantity = self.str_to_float(quantity_str)
                    return quantity
                else:
                    total_products_on_alert = \
                        self.should_be_total_products_on_alert()
                    shipping_on_alert = self.should_be_shipping_on_alert()
                    total_right_on_alert = \
                        self.should_be_total_right_on_alert()
                    time.sleep(2)
                    self.go_to_continue_shopping()
                    if need_basket_quantity is True:
                        baskets_quantity = self.should_be_baskets_quantity()
                    print("ВЫБРАННЫЕ ЗНАЧЕНИЯ ЦЕН: ", quantity_str,
                          element_on_main_page_text,
                          element_on_alert_text,
                          total_products_on_alert,
                          shipping_on_alert,
                          total_right_on_alert)
                    print("ЗНАЧЕНИЯ ЦЕН ДЛЯ ОТПРАВКИ В str_to_float: ",
                          self.delete_overflow_symbols(
                              (quantity_str,
                               element_on_main_page_text,
                               element_on_alert_text,
                               total_products_on_alert,
                               shipping_on_alert,
                               total_right_on_alert)))
                    (quantity, price, sum_left, total_products,
                     shipping, total_right) = \
                        self.str_to_float(*(self.delete_overflow_symbols(
                                          (quantity_str,
                                           element_on_main_page_text,
                                           element_on_alert_text,
                                           total_products_on_alert,
                                           shipping_on_alert,
                                           total_right_on_alert))))
                    if need_basket_quantity is True:
                        print("ФУНКЦИЯ one_element_on_main_page_and_alert"
                              " ПРОЙДЕНА УСПЕШНО")
                        return (price, sum_left, quantity, total_products,
                                shipping, total_right, empty_basket,
                                baskets_quantity)
                    else:
                        print("ФУНКЦИЯ one_element_on_main_page_and_alert"
                              " ПРОЙДЕНА УСПЕШНО")
                        return (price, sum_left, quantity, total_products,
                                shipping, total_right)
            print("ФУНКЦИЯ one_element_on_main_page_and_alert ПРОЙДЕНА"
                  " УСПЕШНО")
            return element_on_main_page_text, element_on_alert_text
        except ElementClickInterceptedException:
            return False

    def go_to_basket_page_from_alert(self, need_text=False):
        self.set_size_window()
        elements_on_main_page_ = self.find_displayed_elements(
            *MainPageLocators.NAME_PRODUCT)
        if need_text is False:
            element_on_main_page = \
                self.one_product_from_list(elements_on_main_page_, False)
        else:
            element_on_main_page, element_on_alert_text = \
                self.one_product_from_list(elements_on_main_page_)
        self.go_to_alert_form(element_on_main_page)
        basket_button_on_alert = self.should_be_basket_button_on_alert()
        assert basket_button_on_alert is not False
        basket_page = self.open_basket_page_on_alert(basket_button_on_alert)
        if basket_page is False and need_text is False:
            return False
        elif basket_page is False and need_text is True:
            return False, False
        elif basket_page is not False and need_text is False:
            return basket_page
        else:
            return basket_page, element_on_alert_text

    def choose_name_on_main_page_and_alert(self):
        return self.one_element_on_main_page_and_alert(
            MainPageLocators.NAME_PRODUCT,
            MainPageLocators.NAME_PRODUCT_ON_ALERT,
            count=1)

    def choose_price_on_main_page_and_alert_some_times(self, count, nbq,
                                                       only_qu=False):
        return self.one_element_on_main_page_and_alert(
            MainPageLocators.PRICE_PRODUCT_UNDER_IMG,
            MainPageLocators.TOTAL_LEFT,
            count, need_quantity=True, need_basket_quantity=nbq,
            only_quantity=only_qu)

    def should_be_elements_on_alert(self):
        try:
            self.set_size_window()
            prices_under_img = self.should_be_price_products_under_img()
            price_on_main_page = \
                self.one_product_from_list(prices_under_img, False)
            self.go_to_alert_form(price_on_main_page)
            alert_form = self.should_be_alert_form()
            successfully_added = self.should_be_successfully_added_text()
            name_product_on_alert = self.should_be_name_product_on_alert()
            quantity_on_alert = self.should_be_quantity_on_alert()
            total_left_on_alert = self.should_be_total_left_on_alert()
            total_products_on_alert = self.should_be_total_products_on_alert()
            shipping_on_alert = self.should_be_shipping_on_alert()
            total_right_on_alert = self.should_be_total_right_on_alert()
            continue_button = self.should_be_continue_button_on_alert()
            basket_button_on_alert = self.should_be_basket_button_on_alert()
            return (alert_form, name_product_on_alert,
                    quantity_on_alert, total_left_on_alert,
                    total_products_on_alert, shipping_on_alert,
                    total_right_on_alert, continue_button,
                    basket_button_on_alert, successfully_added)
        except ElementClickInterceptedException or IndexError:
            return False

    def should_be_img_products(self):
        return self.find_displayed_elements(*MainPageLocators.IMG_PRODUCT)

    def should_be_name_products(self):
        return self.find_displayed_elements(*MainPageLocators.NAME_PRODUCT)

    def should_be_price_products_under_img(self):
        return self.find_displayed_elements(
            *MainPageLocators.PRICE_PRODUCT_UNDER_IMG)

    def should_be_view(self):
        return self.find_displayed_elements(*MainPageLocators.VIEW)

    def should_be_price_products_on_img(self):
        return self.find_displayed_elements(
            *MainPageLocators.PRICE_PRODUCT_ON_IMG)

    def should_be_add_to_cart_button(self):
        return self.find_displayed_elements(
            *MainPageLocators.ADD_TO_CART_UNDER_IMG)

    def should_be_more_button(self):
        return self.find_displayed_elements(*MainPageLocators.MORE_BUTTON)

    def should_be_alert_form(self):
        return self.find_clickable_element(*MainPageLocators.ALERT_FORM)

    def should_be_successfully_added_text(self):
        return self.find_text_element(
            *MainPageLocators.SUCCESSFULLY_ADDED_ON_ALERT)

    def should_be_name_product_on_alert(self):
        return self.find_clickable_element(
            *MainPageLocators.NAME_PRODUCT_ON_ALERT)

    def should_be_quantity_on_alert(self):
        return self.find_text_element(*MainPageLocators.QUANTITY)

    def should_be_total_left_on_alert(self):
        return self.find_text_element(*MainPageLocators.TOTAL_LEFT)

    def should_be_total_products_on_alert(self):
        return self.find_text_element(*MainPageLocators.TOTAL_PRODUCTS)

    def should_be_shipping_on_alert(self):
        return self.find_text_element(*MainPageLocators.SHIPPING)

    def should_be_total_right_on_alert(self):
        return self.find_text_element(*MainPageLocators.TOTAL_RIGHT)

    def should_be_continue_button_on_alert(self):
        return self.find_clickable_element(
            *MainPageLocators.CONTINUE_SHOPPING_BUTTON)

    def should_be_basket_button_on_alert(self):
        return self.find_clickable_element(
            *MainPageLocators.BASKET_BUTTON_ON_ALERT)

    def should_be_empty_basket(self):
        return self.find_text_element(*MainPageLocators.EMPTY_BASKET)

    def should_be_baskets_quantity(self):
        return self.find_text_element(*MainPageLocators.BASKETS_QUANTITY)
