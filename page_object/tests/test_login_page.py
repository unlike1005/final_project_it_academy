import pytest
import random
from page_object.json_reader import datas
from page_object.pages.login_page import LoginPages
from page_object.pages.create_account_page import CreateAccountPage
from page_object.pages.my_account_page import MyAccountPage
from test_main_page import LINK_LOGIN


data_test_login_page = datas.get("test_login_page")
CORRECT_EMAIL = datas.get("correct_email")

links = data_test_login_page.get("links")
LINK_ERROR_AUTHENTICATION = links.get("link_error_authentication")
LINK_SUCCESS_AUTHENTICATION = links.get("link_success_authentication")


def open_login_page(browser):
    login_page = LoginPages(browser, LINK_LOGIN)
    login_page.open(LINK_LOGIN)
    return login_page


def for_negative_tests_login(browser, email, password,
                             error3, error2="There is 1 error"):
    login_page = open_login_page(browser)
    assert login_page is not False
    if login_page is not False:
        error_msg2, error_msg3, url_after_click = \
            login_page.login(email, password)
        assert error_msg2 == error2
        assert error_msg3 == error3
        assert url_after_click in LINK_ERROR_AUTHENTICATION


def test_elements_on_login_page_is_present(browser):
    login_page = open_login_page(browser)
    login_button, email_login_form, password_login_form, \
        msg, create_account_button, email_create_account_form = \
        login_page.should_be_login_page()
    assert login_button is not False
    assert email_login_form is not False
    assert password_login_form is not False
    assert msg == data_test_login_page.get("assert_msg")
    assert create_account_button is not False
    assert email_create_account_form is not False


@pytest.mark.parametrize("email",
                         data_test_login_page.get("emails"))
def test_create_account_incorrect_email(browser, email):
    login_page = open_login_page(browser)
    assert login_page is not False
    if login_page is not False:
        error_msg1, url_after_click = login_page.create_account(email)
        assert error_msg1 == data_test_login_page.get("assert_error_msg1")
        assert login_page.url == url_after_click


def test_create_account_correct_email(browser):
    login_page = open_login_page(browser)
    assert login_page is not False
    if login_page is not False:
        email = "some_user" + str(random.randint(0, 10000)) + "@site.com"
        create_account_page = login_page.create_account(email)
        assert isinstance(create_account_page, CreateAccountPage)
        assert login_page.url != create_account_page.url
        assert create_account_page.url == \
               links.get("link_registration_new_user_form")


@pytest.mark.xfail(reason="когда реализуют сообщение о 2 ошибках,"
                          "добавить аргумент и локатор со второй ошибкой:"
                          "пустой пароль и удалить тест"
                          "test_login_send_empty_forms_one_error")
def test_login_send_empty_forms_will_be(browser):
    for_negative_tests_login(browser, email="", password="",
                             error3="An email address required.",
                             error2="There is 2 error",
                             )


def test_login_send_empty_forms_one_error(browser):
    login_page = open_login_page(browser)
    assert login_page is not False
    if login_page is not False:
        error_msg2, error_msg3, url_after_click = login_page.login("", "")
        assert error_msg2 == data_test_login_page.get("assert_error_msg2")
        assert error_msg3 == data_test_login_page.get("assert_error_msg3")
        assert url_after_click in LINK_ERROR_AUTHENTICATION


@pytest.mark.parametrize("email, password, error3",
                         data_test_login_page.get("emails_passwords_errors3")
                         )
def test_login_negative(browser, email, password, error3):
    for_negative_tests_login(browser, email, password, error3)


def test_login_send_correct_email_and_password(browser):
    login_page = open_login_page(browser)
    assert login_page is not False
    if login_page is not False:
        my_account_page = login_page.login(*CORRECT_EMAIL)
        assert isinstance(my_account_page, MyAccountPage)
        assert login_page.url != my_account_page.url
        assert my_account_page.url == LINK_SUCCESS_AUTHENTICATION


# def test_logo_link(browser):
#     login_page = open_login_page(browser)
#     assert login_page is not False
#     if login_page is not False:
#         logo_link = login_page.should_be_logo_link()
#         assert logo_link is not False
#         if logo_link is not False:
#             back_main_page = login_page.click_logo_link(logo_link)
#             assert isinstance(back_main_page, MainPage)
#             assert login_page.url != back_main_page.url
#             assert back_main_page.url == LINK
