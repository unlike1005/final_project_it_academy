from page_object.json_reader import datas
from page_object.pages.main_page import MainPage
from page_object.pages.login_page import LoginPages
from page_object.pages.basket_page import BasketPage

data_test_main_page = datas.get("test_main_page")

links = data_test_main_page.get("links")
LINK = links.get("link")
LINK_LOGIN = links.get("link_login")
LINK_BASKET = links.get("link_basket")

titles = data_test_main_page.get("titles")
LOGIN_TITLE = titles.get("login_title")
CONTACT_TITLE = titles.get("contact_title")
BASKET_TITLE = titles.get("basket_title")
SEARCH_TITLE = titles.get("search_title")


def open_main_page(browser):
    main_page = MainPage(browser, LINK)
    main_page.open(LINK)
    return main_page


def for_checking_price_and_quantity(browser, count, nbq):
    main_page = open_main_page(browser)
    result = main_page.choose_price_on_main_page_and_alert_some_times(count,
                                                                      nbq)
    assert False not in result

    if nbq is True:
        (price, sum_left, quantity, total_products, shipping,
         total_right, empty_basket, baskets_quantity) = result
    else:
        (price, sum_left, quantity, total_products, shipping,
         total_right) = result

    assert price * quantity == sum_left
    assert sum_left == total_products
    assert shipping == 2
    assert total_right == sum_left + shipping
    if nbq is True:
        assert empty_basket == data_test_main_page.get("assert_empty_basket")
        assert baskets_quantity == str(count)


def test_open_login_page(browser):
    main_page = open_main_page(browser)
    login_link = main_page.should_be_login_link()
    assert login_link is not False
    if login_link is not False:
        login_page = main_page.open_login_page(login_link)
        assert isinstance(login_page, LoginPages)
        assert LOGIN_TITLE in login_page.driver.title
        assert main_page.url != login_page.url
        assert login_page.url == LINK_LOGIN


def test_user_can_open_basket(browser):
    main_page = open_main_page(browser)
    basket_button = main_page.should_be_basket_button()
    assert basket_button is not False
    if basket_button is not False:
        basket_page = main_page.open_basket_page(basket_button)
        assert isinstance(basket_page, BasketPage)
        assert main_page.url != basket_page.url
        assert BASKET_TITLE in basket_page.driver.title
        assert basket_page.url == LINK_BASKET


def test_user_can_open_contact_page(browser):
    main_page = open_main_page(browser)
    contact_link = main_page.should_be_contact_link()
    assert contact_link is not False
    if contact_link is not False:
        contact_page = main_page.open_contact_page(contact_link)
        assert CONTACT_TITLE in contact_page.driver.title


def test_user_can_search(browser):
    main_page = open_main_page(browser)
    search_form, search_button = main_page.should_be_search()
    assert search_form is not False
    assert search_button is not False
    if search_form is not False and search_button is not False:
        search_page = main_page.open_search_page(search_form, search_button)
        assert SEARCH_TITLE in search_page.driver.title


def test_can_user_see_product(browser):
    main_page = open_main_page(browser)
    (imgs, names, prices_under_img, view, price_on_img, add_to_cart_button,
     more_button, price_under_img_text) = \
        main_page.should_be_products()
    assert len(imgs) == len(names)
    assert len(names) == len(prices_under_img)
    assert len(imgs) > 0
    assert len(view) == len(price_on_img) == 1
    assert len(price_on_img) == len(add_to_cart_button) == 1
    assert len(add_to_cart_button) == len(more_button) == 1
    assert price_under_img_text == price_on_img[0].text


def test_elements_on_alert_is_present(browser):
    main_page = open_main_page(browser)
    tuple_of_alerts_objects = main_page.should_be_elements_on_alert()
    assert False not in tuple_of_alerts_objects
    (alert_form, name_product_on_alert, quantity_on_alert, total_left_on_alert,
     total_products_on_alert, shipping_on_alert, total_right_on_alert,
     continue_button, basket_button_on_alert, successfully_added) = \
        tuple_of_alerts_objects
    assert successfully_added == data_test_main_page.get("assert_success_add")
    for i in [total_left_on_alert, total_products_on_alert,
              shipping_on_alert, total_right_on_alert]:
        assert i[0] == datas.get("currency")


def test_name_in_main_page_and_alert(browser):
    main_page = open_main_page(browser)
    result = main_page.choose_name_on_main_page_and_alert()
    assert False not in result
    element_on_main_page, element_on_alert_text = result
    assert element_on_main_page == element_on_alert_text


def test_prace_in_main_page_and_alert_one_time(browser):
    for_checking_price_and_quantity(browser, 1, False)


def test_prace_in_main_page_and_alert_several_times(browser):
    for_checking_price_and_quantity(browser, 2, True)


def test_go_to_basket_page_from_alert(browser):
    main_page = open_main_page(browser)
    basket_page = main_page.go_to_basket_page_from_alert()
    assert isinstance(basket_page, BasketPage)
    assert BASKET_TITLE in basket_page.driver.title
    assert main_page.url != basket_page.url
    assert basket_page.url == LINK_BASKET
