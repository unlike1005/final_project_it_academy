from page_object.json_reader import datas
from page_object.tests.test_login_page import open_login_page, CORRECT_EMAIL


data_test_my_account_page = datas.get("test_my_account_page")


def open_my_account_page(browser):
    login_page = open_login_page(browser)
    my_account_page = login_page.login(*CORRECT_EMAIL)
    return my_account_page


def test_elements_on_account_page_is_presents(browser):
    my_account_page = open_my_account_page(browser)
    text_my_account_up = my_account_page.should_be_text_my_account_up()
    text_my_account_down = my_account_page.should_be_text_my_account_down()
    text_info_account = my_account_page.should_be_text_info_account()
    order_button = my_account_page.should_be_order_button()
    credit_button = my_account_page.should_be_credit_button()
    address_button = my_account_page.should_be_address_button()
    personal_info_button = my_account_page.should_be_personal_info_button()
    wishlists_button = my_account_page.should_be_wishlists_button()
    home_button_down = my_account_page.should_be_home_button_down()
    assert text_my_account_up == \
           data_test_my_account_page.get("assert_text_my_account_up")
    assert text_my_account_down == \
           data_test_my_account_page.get("assert_text_my_account_down")
    assert text_info_account == \
           data_test_my_account_page.get("assert_text_info_account")
    assert order_button is not False
    assert credit_button is not False
    assert address_button is not False
    assert personal_info_button is not False
    assert wishlists_button is not False
    assert home_button_down is not False
