# from pages.create_account_page import CreateAccountPage
from test_login_page import open_login_page
import random


def go_to_create_account_page(browser):
    login_page = open_login_page(browser)
    assert login_page is not False
    if login_page is not False:
        email = "kolobok" + str(random.randint(0, 10000)) + "@kolobok.com"
        create_account_page = login_page.create_account(email)
        return create_account_page
    else:
        return False
