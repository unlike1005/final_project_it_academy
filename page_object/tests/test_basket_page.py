from page_object.pages.basket_page import BasketPage
from page_object.pages.main_page import MainPage
from page_object.json_reader import datas
from test_main_page import LINK, LINK_BASKET, open_main_page
from selenium.common.exceptions import ElementClickInterceptedException
import time


data_test_login_page = datas.get("test_basket_page")
MAIN_TITLE = data_test_login_page.get("main_title")


def open_basket_page(browser):
    basket_page = BasketPage(browser, LINK_BASKET)
    basket_page.open(LINK_BASKET)
    return basket_page


def click_on_home_button(basket_page):
    home_button = basket_page.should_be_home_button()
    assert home_button is not False
    try:
        home_button.click()
        return MainPage(basket_page.driver, basket_page.driver.current_url)
    except ElementClickInterceptedException:
        return False


def choose_product(main_page, need_text):
    if need_text is True:
        basket_page, name_product_in_alert_text = \
            main_page.go_to_basket_page_from_alert(need_text=True)
        return basket_page, name_product_in_alert_text
    else:
        basket_page = main_page.go_to_basket_page_from_alert(need_text=False)
        return basket_page


def test_elements_in_login_page_is_present(browser):
    basket_page = open_basket_page(browser)
    home_button, shopping_cart, cart_summary, steps, empty_warning = \
        basket_page.should_be_elements_on_busket_page()
    assert home_button is not False
    assert shopping_cart == data_test_login_page.get("assert_shopping_cart")
    assert cart_summary == data_test_login_page.get("assert_cart_summary")
    assert steps is not False
    assert empty_warning == data_test_login_page.get("empty_warning")


def test_home_button(browser):
    basket_page = open_basket_page(browser)
    main_page = click_on_home_button(basket_page)
    assert isinstance(main_page, MainPage)
    assert MAIN_TITLE in main_page.driver.title
    assert basket_page.url != main_page.url
    assert main_page.url == LINK


def test_names_in_alert_and_basket_page(browser):
    main_page = open_main_page(browser)
    basket_page, name_product_in_alert_text = \
        choose_product(main_page, need_text=True)
    name_product_in_basket = basket_page.should_be_product_name()
    assert name_product_in_basket == name_product_in_alert_text


def test_elements_is_present_on_basket_and_equal_on_main_page_alert(
        browser):
    main_page = open_main_page(browser)
    (price_alert, sum_left_alert, quantity_alert, total_products_alert,
     shipping_alert, total_right_alert) = \
        main_page.choose_price_on_main_page_and_alert_some_times(count=3,
                                                                 nbq=False)
    basket_button = main_page.should_be_basket_button()
    assert basket_button is not False
    basket_page = main_page.open_basket_page(basket_button)
    time.sleep(2)
    result = basket_page.should_by_prices_and_quantity_on_basket_page(
        need_tax=False)
    assert False not in result
    (unit_price, qty, total_one_name, total_products, total_shipping,
     total_without_tax) = result
    for i in (unit_price, total_one_name, total_products, total_shipping,
              total_without_tax):
        assert i[0][0] == '$'
    (unit_price_float, qty_float, total_one_name_float, total_products_float,
     total_shipping_float, total_without_tax_float) = \
        basket_page.str_to_float(unit_price[0][1:], qty[0],
                                 total_one_name[0][1:], total_products[1:],
                                 total_shipping[1:],
                                 total_without_tax[1:])
    assert price_alert == unit_price_float
    assert sum_left_alert == total_one_name_float
    assert total_products_alert == total_products_float
    assert quantity_alert == qty_float
    assert shipping_alert == total_shipping_float
    assert total_right_alert == total_without_tax_float


def choose_two_products(browser):
    main_page1 = open_main_page(browser)
    basket_page1, name_product1 = choose_product(main_page1, need_text=True)
    main_page2 = click_on_home_button(basket_page1)
    basket_page2, name_product2 = choose_product(main_page2, need_text=True)
    return name_product1, name_product2, basket_page2


def click_on_delete_buttons(basket_page):
    delete_product_buttons = basket_page.should_be_delete_product_buttons()
    if delete_product_buttons is not False:
        basket_page.click_to_delete_button(delete_product_buttons)
    else:
        return False


def test_delete_button(browser):
    main_page = open_main_page(browser)
    times = 2
    quantity1 = \
        main_page.choose_price_on_main_page_and_alert_some_times(
            count=times, nbq=False, only_qu=True)
    basket_button = main_page.should_be_basket_button_on_alert()
    basket_page = main_page.open_basket_page_on_alert(basket_button)
    time.sleep(3)
    del_but = basket_page.should_be_delete_product_buttons()
    assert del_but is not False
    result = basket_page.click_to_delete_button(del_but)
    assert result is True
    empty_warning = basket_page.should_be_empty_warning()
    try:
        quantity1_int = int(*quantity1[0])
    except TypeError:
        return False
    assert quantity1_int == times
    assert empty_warning is not False


def test_math_on_basket(browser):
    name_product1 = name_product2 = ""
    while name_product1 == name_product2:
        name_product1, name_product2, basket_page = \
            choose_two_products(browser)
        if name_product1 == name_product2:
            del_but = basket_page.should_be_delete_product_buttons()
            basket_page.click_to_delete_button(del_but)
    time.sleep(3)
    qty1 = basket_page.checking_math()
    icon_plus = basket_page.should_be_icon_plus()
    assert icon_plus is not False
    assert len(icon_plus) == 2
    icon_plus_click = basket_page.click_on_icon_plus(icon_plus[0])
    assert icon_plus_click is True
    time.sleep(3)
    qty2 = basket_page.checking_math()
    assert (sum(qty2) - sum(qty1)) == 1
    icon_minus = basket_page.should_be_icon_minus()
    assert icon_plus is not False
    assert len(icon_plus) == 2
    icon_minus_click = basket_page.click_on_icon_minus(icon_minus[1])
    assert icon_minus_click is True
    time.sleep(3)
    qty3 = basket_page.checking_math()
    assert (sum(qty2) - sum(qty3)) == 1
