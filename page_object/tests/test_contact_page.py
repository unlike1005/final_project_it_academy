# from page_object.pages.contact_page import ContactPage
# from page_object.locators.contact_page_locators import ContactPageLocators
from test_main_page import open_main_page
# from page_object.pages.main_page import should_be_contact_link,
# open_contact_page


def go_to_contact_page(browser):
    main_page = open_main_page(browser)
    contact_link = main_page.should_be_contact_link()
    if contact_link is False:
        return False
    else:
        contact_page = main_page.open_contact_page(contact_link)
        return contact_page
