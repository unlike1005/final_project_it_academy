from pytest import fixture
from selenium import webdriver


@fixture()
def browser():
    browser = webdriver.Chrome()
    yield browser
    browser.close()
