from selenium.webdriver.common.by import By


class LoginPageLocators:
    CREATE_ACCOUNT_BUTTON = (By.ID, 'SubmitCreate')
    EMAIL_CREATE_ACCOUNT_FORM = (By.ID, 'email_create')
    MSG1 = (By.CSS_SELECTOR, '.form_content.clearfix p')
    ERROR_MSG1 = (By.CSS_SELECTOR, '#create_account_error li')

    SIGN_IN_BUTTON = (By.ID, 'SubmitLogin')
    EMAIL_LOGIN_FORM = (By.ID, 'email')
    PASSWORD_LOGIN_FORM = (By.ID, 'passwd')
    ERROR_MSG2 = (By.CSS_SELECTOR, '.alert-danger p')
    ERROR_MSG3 = (By.CSS_SELECTOR, '.alert-danger li')

    # LOGO_LINK = (By.CSS_SELECTOR, '.logo.img-responsive')
