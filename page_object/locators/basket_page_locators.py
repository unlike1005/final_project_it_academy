from selenium.webdriver.common.by import By


class BasketPageLocators:
    HOME_BUTTON = (By.CLASS_NAME, "home")
    SHOPPING_CART = (By.CSS_SELECTOR, "span.navigation_page")
    SHOPPING_CART_SUMMARY = (By.ID, "cart_title")
    STEPS = (By.CSS_SELECTOR, ".step.clearfix")
    EMPTY_WARNING = (By.CSS_SELECTOR, ".alert.alert-warning")

    PRODUCT_NAME = (By.XPATH, "//td[@class='cart_description']/p/a")
    UNIT_PRICE = (By.XPATH,
                  "//td[@class='cart_unit']/span[@class='price']/span[@class!"
                  "='price-percent-reduction small' and @class!='old-price']")
    QTY = (By.CSS_SELECTOR, ".cart_quantity input.cart_quantity_input[value]")
    TOTAL_ONE_NAME = (By.CSS_SELECTOR, "td[data-title='Total'] span")
    TOTAL_PRODUCTS = (By.CSS_SELECTOR, "tfoot td#total_product")
    TOTAL_SHIPPING = (By.CSS_SELECTOR, "tr.cart_total_delivery td.price")
    TOTAL_WITHOUT_TAX = (By.CSS_SELECTOR,
                         "tr.cart_total_price td.price"
                         "#total_price_without_tax")
    TAX = (By.CSS_SELECTOR, "tr.cart_total_tax td#total_tax")
    TOTAL_WITH_TAX = (By.CSS_SELECTOR, "tr.cart_total_price span#total_price")

    ICON_PLUS = (By.CSS_SELECTOR, "i.icon-plus")
    ICON_MINUS = (By.CSS_SELECTOR, "i.icon-minus")
    DELETE_PRODUCT_BUTTONS = (By.CSS_SELECTOR, "div a.cart_quantity_delete")
