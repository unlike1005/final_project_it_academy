from selenium.webdriver.common.by import By


class BasePageLocators:
    LOGIN_LINK = (By.CLASS_NAME, 'login')
    CONTACT_US_LINK = (By.ID, 'contact-link')
    BASKET_BUTTON = (By.CSS_SELECTOR, 'a[title="View my shopping cart"]')
    SEARCH_FORM = (By.CLASS_NAME, 'search_query')
    SEARCH_BUTTON = (By.CSS_SELECTOR, '[name="submit_search"]')
