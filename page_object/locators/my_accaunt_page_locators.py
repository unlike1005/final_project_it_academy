from selenium.webdriver.common.by import By


class MyAccountLocators:
    TEXT_MY_ACCOUNT_UP = (By.CLASS_NAME, "navigation_page")
    TEXT_MY_ACCOUNT_DOWN = (By.CLASS_NAME, "page-heading")
    INFO_ACCOUNT = (By.CLASS_NAME, "info-account")
    ORDER_BUTTON = (By.CSS_SELECTOR, "a[title='Orders']")
    CREDIT_BUTTON = (By.CSS_SELECTOR, "a[title='Credit slips']")
    ADDRESS_BUTTON = (By.CSS_SELECTOR, "a[title='Addresses']")
    PERSONAL_INFO_BUTTON = (By.CSS_SELECTOR, "a[title='Information']")
    WISHLISTS_BUTTON = (By.CSS_SELECTOR, "a[title='My wishlists']")
    HOME_BUTTON_DOWN = (By.CSS_SELECTOR, "a[title='Home']")
