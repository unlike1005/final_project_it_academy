from selenium.webdriver.common.by import By


class MainPageLocators:
    IMG_PRODUCT = (By.CSS_SELECTOR, 'img.replace-2x.img-responsive')
    NAME_PRODUCT = (By.CSS_SELECTOR, 'a.product-name')
    PRICE_PRODUCT_UNDER_IMG = (By.CSS_SELECTOR,
                               '.right-block .price.product-price')

    VIEW = (By.CSS_SELECTOR, '.quick-view')
    PRICE_PRODUCT_ON_IMG = (By.CSS_SELECTOR,
                            '.left-block .price.product-price')
    ADD_TO_CART_UNDER_IMG = (By.XPATH, '//span[text()="Add to cart"]')
    MORE_BUTTON = (By.CSS_SELECTOR, '[title="View"] span')

    ALERT_FORM = (By.ID, 'layer_cart')
    SUCCESSFULLY_ADDED_ON_ALERT = (By.CSS_SELECTOR, '.clearfix h2')
    NAME_PRODUCT_ON_ALERT = (By.CSS_SELECTOR, '#layer_cart_product_title')
    QUANTITY = (By.ID, 'layer_cart_product_quantity')
    TOTAL_LEFT = (By.ID, 'layer_cart_product_price')
    TOTAL_PRODUCTS = (By.CLASS_NAME, 'ajax_block_products_total')
    SHIPPING = (By.CSS_SELECTOR, '.layer_cart_row .ajax_cart_shipping_cost')
    TOTAL_RIGHT = (By.CSS_SELECTOR, '.layer_cart_row .ajax_block_cart_total')
    CONTINUE_SHOPPING_BUTTON = (By.CSS_SELECTOR, '.layer_cart_cart .continue')
    BASKET_BUTTON_ON_ALERT = (By.CLASS_NAME, 'button-medium')

    EMPTY_BASKET = (By.CSS_SELECTOR,
                    '.shopping_cart span.ajax_cart_no_product')
    BASKETS_QUANTITY = (By.CSS_SELECTOR,
                        '.shopping_cart span.ajax_cart_quantity')
