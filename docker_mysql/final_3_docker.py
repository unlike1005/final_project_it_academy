from subprocess import Popen, PIPE
import docker
import mysql.connector as mysql
import time
import json


def command_to_terminal(command):
    process = Popen(command, stdout=PIPE, stderr=PIPE, shell=True,
                    universal_newlines=True)
    exit_code = process.wait()
    stdout, stderr = process.communicate()
    if exit_code == 0:
        print(f'контейнер запустился, id контенера {stdout}')
    else:
        print(f'exec_code: {exit_code}\nstderr: {stderr}')
        quit()


def traffic_lights(container):
    flag = False
    count = 0
    while flag is False:
        if 'port: 3306  MySQL' in str(container.logs(), 'utf-8'):
            flag = True
        else:
            count += 1
            print(count)
            time.sleep(10)


def start_container():
    client = docker.from_env()
    client.images.pull("mysql")
    command = 'docker run --name docker_mysql -p 127.0.0.1:1111:3306' \
              ' -e MYSQL_ROOT_PASSWORD=q -d mysql'
    command_to_terminal(command)
    container = client.containers.get('docker_mysql')
    traffic_lights(container)
    return container


def connect_to_db():
    db = mysql.connect(
        host='127.0.0.1',
        port='1111',
        user="root",
        password="q"
    )
    return db, db.cursor()


def get_queries():
    with open("queries.json", "r", encoding='utf-8') as f:
        data_from_json = json.load(f)
    return data_from_json


def create_tables(cursor, queries):
    for query in queries:
        one_execute(cursor, queries.get(query), to_print=False)


def print_task_and_insert_values_to_tables(cursor, insert_queries_and_values):
    for y in insert_queries_and_values:
        print((insert_queries_and_values.get(y)).get('tasks'))
        query = (insert_queries_and_values.get(y)).get('query')
        values = (insert_queries_and_values.get(y)).get('values')
        values_list_of_tuples = []
        for z in values:
            values_list_of_tuples.append(tuple(z))
        many_execute(cursor, query, values_list_of_tuples,
                     text_for_print="записей добавлено в таблицу")


def one_execute(cursor, query, to_print=True):
    cursor.execute(query)
    if to_print:
        print(cursor.fetchall())


def many_execute(cursor, query, values, to_print=True, **text_for_print):
    cursor.executemany(query, values)
    if to_print:
        print(cursor.rowcount, text_for_print)


def print_task_and_execute(cursor, task_and_query):
    for one_tq in task_and_query:
        print(one_tq.get("task"))
        one_execute(cursor, one_tq.get("query"))


def print_one_task_and_some_execute(cursor, task_and_queries):
    print(task_and_queries.get('task'))
    queries = task_and_queries.get('queries')
    for i in range(len(queries)):
        one_execute(cursor, queries[i], to_print=False)


def main():
    container = start_container()
    db, cursor = connect_to_db()

    data = get_queries()

    cursor.execute((data.get("selects0")).get("create_db"))
    cursor.execute((data.get("selects0")).get("use_db"))
    create_tables(cursor, data.get("create_tables"))
    one_execute(cursor, (data.get("selects0")).get("show_tables"))
    print_task_and_insert_values_to_tables(cursor, data.get("inserts"))
    print_task_and_execute(cursor, data.get("selects1"))
    print_task_and_insert_values_to_tables(cursor, data.get("add_rows"))
    print_task_and_execute(cursor, data.get("selects2"))
    print_one_task_and_some_execute(cursor, data.get("alt_upd"))
    one_execute(cursor, (data.get("selects0")).get("customers_columns"))
    one_execute(cursor, (data.get("selects0")).get("customers_values"))
    print_task_and_execute(cursor, data.get("selects3"))

    db.commit()
    db.close()
    container.stop()


main()
